# Template

![SW Version](https://img.shields.io/badge/dynamic/json?color=informational&label=SW+version&query=%24.general.version.sw&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)
![PCB Version](https://img.shields.io/badge/dynamic/json?color=informational&label=PCB+version&query=%24.general.version.pcb&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)
![CAD Version](https://img.shields.io/badge/dynamic/json?color=informational&label=CAD+version&query=%24.general.version.cad&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)
[![SW License](https://img.shields.io/badge/dynamic/json?color=green&label=SW+license&query=%24.general.license.sw&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)](https://codeberg.org/atlas144/-----REPO-----/src/branch/main/SW_LICENSE)
[![HW License](https://img.shields.io/badge/dynamic/json?color=green&label=HW+license&query=%24.general.license.hw&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)](https://codeberg.org/atlas144/-----REPO-----/src/branch/main/HW_LICENSE)
![Language](https://img.shields.io/badge/dynamic/json?color=red&label=language&query=%24.general.language&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

## Description

The M.A.L.S. LTS is a line detection sensor designed for the M.A.L.S. open robotic vehicle. It is the primary data source for its line tracking control method.

The sensor uses the TWI bus for communication, so it can be used in any device with this bus.

### Working principle

The sensor consists of three CNY70 reflective optocouplers that sense the reflectivity of the surface below the sensor. The acquired values (in the range 0 - 255) are then sent to the requester on demand via the TWI bus.

## Using

The sensor must be placed as close as possible to the sensed surface (according to the datasheet, the ideal distance is 0.3 mm), of course taking into account the roughness of the surface. Then the power wires must be connected to the power pins and the data pins to the TWI bus.

After that, a TWI call can be used to request data from the sensor at address **0x23**. The format of the response is following:

| Byte | Possible values | Description |
| ---- | --------------- | ----------- |
| 0 | 0 - 255 | left optocouplers measured reflectivity (0 = absolute black, 255 = theoretical brightest white) |
| 1 | 0 - 255 | center optocouplers measured reflectivity (0 = absolute black, 255 = theoretical brightest white) |
| 2 | 0 - 255 | right optocouplers measured reflectivity (0 = absolute black, 255 = theoretical brightest white) |

## Dependencies

- [Wire](https://www.arduino.cc/reference/en/language/functions/communication/wire/) \[1.x.x\] - TWI communication library

## Contributing

Contributions are welcome!

## License

The source code is licensed under the [MIT](https://codeberg.org/atlas144/-----REPO-----/src/branch/main/SW_LICENSE) license.

The hardware files are licensed under the [CERN Open Hardware License Version 2 - Strongly Reciprocal](https://codeberg.org/atlas144/-----REPO-----/src/branch/main/HW_LICENSE).
